import Dashboard from "./Dashboard/Dashboard";
import AgentPropertyListPage from "./AgentPropertyListPage/AgentPropertyListPage";
import AddPropertyPage from "./AddPropertyPage/AddPropertyPage";
import EditPropertyPage from "./EditPropertyPage/EditPropertyPage";

import AgentAgenceListPage from "./AgentAgenceListPage/AgentAgenceListPage";
import AddAgencePage from "./AddAgencePage/AddAgencePage";
import EditAgencePage from "./EditAgencePage/EditAgencePage";

import AgentMenu from "./AgentMenu";

export {
  AddPropertyPage,
  AddAgencePage,
  Dashboard,
  AgentPropertyListPage,
  EditPropertyPage,
  AgentAgenceListPage,
  EditAgencePage,
  AgentMenu
};
