import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../../../store/actions";
import { Pagination } from "../../../components";
import AgencePageUI from "./AgencePageUI";
import { AgentMenu } from "..";
import { Spinner } from "reactstrap";

class AgentAgenceListPage extends Component {
  state = {
    currentPage: 1,
    pageSize: 5,
    selectedFilter: "all"
  };
  componentWillMount() {
    this.props.getUserAgenceList(this.state.currentPage, this.state.pageSize);
  }
  componentWillUnmount() {
    this.props.clearError();
    this.props.clearAgence();
  }
  handlePageChange = page => {
    this.setState({ currentPage: page });
    this.props.getUserAgenceList(page, this.state.pageSize);
  };
  deleteAgence = id => {
    this.props.deleteAgence(id);
  };
  render() {
    const { currentPage, pageSize } = this.state;

    let renderComponent;
    const { totalCount1, agencies } = this.props.agence;

    if (Object.keys(agencies).length > 0) {
      renderComponent = (
        <AgencePageUI
          deleteAgence={this.deleteAgence}
          dataList={agencies}
        />
      );
    } else {
      renderComponent = (
        <div
          style={{ width: "100%", height: "100vh" }}
          className="d-flex align-items-center justify-content-center"
        >
          <Spinner color="primary" />
        </div>
      );
    }

    if (Object.keys(this.props.errors).length > 0) {
      renderComponent = <p>no agencies found</p>;
    }

    return (
      <div className="container-fluid">
        <div className="row">
          {/* left section */}
          <AgentMenu />
          {/* <!-- right section --> */}
          <div className="m-auto col-lg-8 col-md-8 col-sm-12  p-2 ">
            {/* <!-- Agencies List --> */}
            <p className="display-4">Total agencies: {totalCount1}</p>
            {renderComponent}
            <Pagination
              itemsCount={totalCount1}
              pageSize={pageSize}
              currentPage={currentPage}
              onPageChange={this.handlePageChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToPrope = state => {
  return {
    errors: state.errors,
    agence: state.agence
  };
};

export default connect(
  mapStateToPrope,
  actions
)(AgentAgenceListPage);
