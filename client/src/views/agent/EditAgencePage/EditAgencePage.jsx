import React, { Component } from "react";
import axios from "axios";

class Edit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      agence: {},
    };
  }

  componentDidMount() {
    axios.get("/api/agence/" + this.props.match.params.id).then((res) => {
      this.setState({ agence: res.data });
      console.log(this.state.agence);
    });
  }

  onChange = (e) => {
    const state = this.state.agence;
    state[e.target.name] = e.target.value;
    this.setState({ agence: state });
  };

  onSubmit = (e) => {
    e.preventDefault();

    const { nameAg, cityAg, imgUrl } = this.state.agence;

    axios
      .put("/api/agence/update/" + this.props.match.params.id, {
        nameAg,
        cityAg,
        imgUrl,
      })
      .then((result) => {
        this.props.history.push("/agent/dashboard");
      });
  };

  render() {
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">EDIT Agence</h3>
          </div>
          <div class="panel-body">
            <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="nameAg">Name:</label>
                <input
                  type="text"
                  class="form-control"
                  name="nameAg"
                  value={this.state.agence.nameAg}
                  onChange={this.onChange}
                  placeholder="Name"
                />
              </div>
              <div class="form-group">
                <label for="imgUrl">ImgUrl:</label>
                <input
                  type="text"
                  class="form-control"
                  name="imgUrl"
                  value={this.state.agence.imgUrl}
                  onChange={this.onChange}
                  placeholder="Img"
                />
              </div>
              <div class="form-group">
                <label for="cityAg">City:</label>
                <input
                  type="text"
                  class="form-control"
                  name="cityAg"
                  value={this.state.agence.cityAg}
                  onChange={this.onChange}
                  placeholder="City"
                />
              </div>

              <button type="submit" class="btn btn-default">
                Submit
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Edit;
