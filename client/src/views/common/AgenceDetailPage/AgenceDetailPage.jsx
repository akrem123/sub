import React from "react";
import { connect } from "react-redux";
import * as actions from "../../../store/actions";
import AgenceDetailUI from "./AgenceDetailUI";
import { Spinner } from "reactstrap";

class AgenceDetailPage extends React.Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.getAgence(id, this.props.history);
  }

  render() {
    const { agence } = this.props.agence;
    let renderComponent;

    if (Object.keys(agence).length > 0) {
      renderComponent = (
        <AgenceDetailUI
          agence={agence}
          agent={this.props.profile.profile}
        />
      );
    } else {
      renderComponent = (
        <div
          style={{ width: "100%", height: "100vh" }}
          className="d-flex align-items-center justify-content-center"
        >
          <Spinner color="primary" />
        </div>
      );
    }

    return <div className="container">{renderComponent}</div>;
  }
}

const mapStateToProps = state => {
  return {
    profile: state.profile,
    agence: state.agence
  };
};

export default connect(
  mapStateToProps,
  actions
)(AgenceDetailPage);
