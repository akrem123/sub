import React from "react";
import { connect } from "react-redux";
import * as actions from "../../../store/actions";
import { CardFour, Pagination } from "../../../components";
import { Spinner } from "reactstrap";

class AgenceListPage extends React.Component {
  state = {
    currentPage: 1,
    pageSize: 5,
    selectedFilter: "all"
  };

  componentDidMount() {
    this.props.getAllAgencies(
      this.state.currentPage,
      this.state.pageSize,
      this.state.selectedFilter
    );
  }
  componentWillUnmount() {
    this.props.clearAgence();
  }
  onInputChange = e => {
    this.setState({
      [e.currentTarget.name]: e.currentTarget.value
    });

    if (e.currentTarget.value === "all") {
      this.props.getAllAgencies(1, 5, "all");
    } else if (e.currentTarget.value === "sousse") {
      this.props.getAllAgencies(1, 5, "sousse");
    } else if (e.currentTarget.value === "mahdia") {
      this.props.getAllAgencies(1, 5, "mahdia");
    }
    else {
      this.props.getAllAgencies(1, 5, "monastir");
    }
  };
  handlePageChange = page => {
    this.setState({ currentPage: page });
    this.props.getAllAgencies(
      page,
      this.state.pageSize,
      this.state.selectedFilter
    );
  };

  render() {
    let { pageSize, currentPage } = this.state;

    const { totalCount1, agencies } = this.props.agence;

    let renderComponent;

    if (agencies.length > 0 && Object.keys(this.props.errors).length === 0) {
      renderComponent = agencies.map(agence => {
        return (
          <CardFour
            key={agence._id}
            nameAg={agence.nameAg}
            agenceId={agence._id}
            agentId={agence.user._id}
            agentName={agence.user.name}
            imgUrl={agence.imgUrl}
            cityAg={agence.cityAg}
          />
        );
      });
    } else {
      if (Object.keys(this.props.errors).length > 0) {
        renderComponent = (
          <p className="d-flex justify-content-center align-items-center text-warning display-4">
            No result for selected Filter
          </p>
        );
      } else {
        renderComponent = (
          <div
            style={{ width: "100%", height: "100vh" }}
            className="d-flex align-items-center justify-content-center"
          >
            <Spinner color="primary" />
          </div>
        );
      }
    }

    return (
      <div className="container">
        <h1 className="display-4 my-5 text-center">
          Agencies List:{totalCount1}
        </h1>
        <div className="row">
          <div className="col-6">
            <form>
              <div className="form-group">
                <label htmlFor="exampleFormControlSelect1">
                  Filter Agencies:
                </label>
                <select
                  name="selectedFilter"
                  onChange={this.onInputChange}
                  className="form-control"
                >
                  {" "}
                  <option value="all">All</option>
                  <option value="sousse">Sousse</option>
                  <option value="mahdia">Mahdia</option>
                  <option value="monastir">Monastir</option>
                </select>
              </div>
            </form>
          </div>
        </div>
        <div className="cards my-5">{renderComponent}</div>

        <Pagination
          itemsCount={totalCount1}
          pageSize={pageSize}
          currentPage={currentPage}
          onPageChange={this.handlePageChange}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    errors: state.errors,
    agence: state.agence
  };
};

export default connect(
  mapStateToProps,
  actions
)(AgenceListPage);
