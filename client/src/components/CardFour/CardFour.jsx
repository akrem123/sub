import React from "react";
import { Link } from "react-router-dom";

class CardFour extends React.Component {
  render() {
    const {
      nameAg,
      agentId,
      agentName,
      cityAg,
      imgUrl,
      text,
      agenceId
    } = this.props;
    

    return (
      <React.Fragment>
        <div className="row">
          <div className="row shadow-sm border p-4 mb-3">
            <div className="col-lg-4 col-md-4 col-sm-8">
              <h4>{nameAg}</h4>
              <img className="card-img-top" src={imgUrl} alt="Card cap" />
            </div>
            <div className="col-lg-8 col-md-8  col-sm-8">
              <strong>City:</strong>
              <Link
                to={`/agent-profile/${agentId}`}
                className="float-right badge badge-primary p-2"
              >
                Agent: {agentName}
              </Link>
              <br />
              <small className="text-muted">{cityAg}</small>
              <div className="row">
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <br />
                  <p className="my-3">{text}</p>
                  <Link
                    to={`/agence-detail/${agenceId}`}
                    className="btn btn-primary"
                  >
                    Visit Agency
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CardFour;
