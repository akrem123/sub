import React from "react";
import { Link } from "react-router-dom";

class CardThree extends React.Component {
  render() {
    const {
      img,
      nameAg,
      cityAg,  
      agenceId,
      btnText
    } = this.props;

    return (
      <div
        className="card shadow-lg"
        style={{
          width: "340px"
        }}
      >
        <img
          className="card-img-top"
          style={{ width: "100%", height: "250px" }}
          src={img}
          alt="Agence"
        />
        <div className="card-body">
          <h5 className="card-title">
            {nameAg}
            <span className="float-right badge badge-success">
              City: {cityAg}
            </span>
          </h5>
          <Link
            to={`/agence-detail/${agenceId}`}
            className="btn btn-primary mt-3"
          >
            {btnText}
          </Link>
        </div>
      </div>
    );
  }
}

export default CardThree;
